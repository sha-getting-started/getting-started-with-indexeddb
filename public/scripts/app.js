'use strict';

// Check if IndexedDb exists in browser.
if(!window.indexedDB) {
    console.log('IndexedDb is not supported by browser');
} else {
    setTimeout(async () => {
        // Write data to store.
        await write('database', 'store', 'key001', { text: 'Sample record 001'});
        await write('database', 'store', 'key002', { text: 'Sample record 002'});

        // Read single record.
        let results = await readSingle('database', 'store', 'key001');
        console.log(results);

        // Remove single record.
        await removeSingle('database', 'store', 'key001');

        // Read all records from a store.
        results = await read('database', 'store');
        console.log(results);

        // Remove all records from store.
        await remove('database', 'store');
    }, 1000);
}